import click
import structlog
import logging


@click.group("squirrel")
def squirrel():
    structlog.configure(
        processors=[
            structlog.processors.TimeStamper("iso"),
            structlog.processors.add_log_level,
            structlog.dev.ConsoleRenderer(),
        ],
        wrapper_class=structlog.make_filtering_bound_logger(logging.INFO),
        logger_factory=structlog.PrintLoggerFactory(),
        cache_logger_on_first_use=True,
    )


from . import encode as _
