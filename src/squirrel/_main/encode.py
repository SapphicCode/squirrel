import glob
import hashlib
import logging
import os
import subprocess
import typing as t
import uuid

import click
import orjson
import structlog

from . import squirrel
from .._utils import age, age_keygen, exiftool, zstd


@squirrel.command()
@click.option(
    "--rm/--no-rm",
    show_default=True,
    default=True,
    help="remove source files",
)
@click.option(
    "--rename-hash/--no-rename-hash",
    help="rename the output file to its hash sum !1 [1]",
)
@click.option(
    "--rename-uuid/--no-rename-uuid",
    help="rename the output file to a UUIDv4 !1",
)
@click.option(
    "-o",
    "--out-dir",
    default=os.path.join(os.getcwd(), "blobs"),
    show_default="$CWD/blobs",
    help="output directory for renamed blobs",
)
@click.argument("file", nargs=-1, required=True)
def encode(**options):
    """
    Encodes FILEs to archive format.

    !n: These options conflict.

    [1] This may create encryption conflicts. Try all known keys when decoding a file by hash.
    """

    logger = structlog.stdlib.get_logger("squirrel.encode")

    logger.debug("Running with options.", options=options)

    input_files: t.List[str] = options["file"]
    rm: bool = options["rm"]
    rename_uuid: bool = options["rename_uuid"]
    rename_hash: bool = options["rename_hash"]
    out_dir: str = options["out_dir"]
    assert not (rename_uuid and rename_hash)

    files: t.List[str] = []
    for ifile in input_files:
        if glob.has_magic(ifile):
            files.extend(
                x for x in glob.glob(ifile, recursive=True) if not os.path.isdir(x)
            )
            continue
        if os.path.isdir(ifile):
            for parent, _, wfiles in os.walk(ifile):
                files.extend(os.path.join(parent, file) for file in wfiles)
            continue
        files.append(ifile)

    logger.info(f"Asked to encode {len(files)} files.", files=files)

    for file in files:
        # pre-flight checks
        if file.endswith(".sqjson"):
            logger.debug("Refusing to encode own metadata file.", file=file)
            continue
        basename = os.path.basename(file)
        if basename.startswith("._") or basename in ["desktop.ini", ".DS_Store"]:
            logger.debug("Ignoring system metadata file.", file=file)
            continue

        logger.info("Encoding file.", file=file)
        metadata_file = f"{file}.sqjson"
        if os.path.exists(metadata_file):
            raise FileExistsError(metadata_file)

        # gather file metadata
        metadata = {}
        metadata["path"] = file
        stat = os.stat(file)
        metadata["size"] = stat.st_size
        metadata["mode"] = stat.st_mode
        metadata["uid"] = stat.st_uid
        metadata["gid"] = stat.st_gid
        metadata["ctime"] = stat.st_ctime
        metadata["mtime"] = stat.st_mtime
        metadata["atime"] = stat.st_atime

        # hash
        hash_meta = {}
        h = hashlib.sha256()
        with open(file, "rb") as f:
            while data := f.read(1024 * 1024):  # 1MiB
                h.update(data)
        hash_meta[h.name] = h.hexdigest()
        assert "sha256" in hash_meta
        metadata["hash"] = hash_meta

        # gather exif metadata where available
        if sum(
            file.lower().endswith(x)
            for x in [
                ".jpg",
                ".jpeg",
                ".heic",
                ".cr2",
                ".dng",
            ]
        ):
            try:
                metadata["exiftool"] = exiftool(file)
                logger.debug("Gathered EXIF metadata successfully.", file=file)
            except subprocess.CalledProcessError as e:
                logger.error("Could not gather EXIF metadata.", exc_info=e)

        # end of file-specific metadata
        metadata: t.Dict[str, t.Any] = {"file": metadata}

        # fingerprint
        metadata["uuid"] = str(uuid.uuid4())

        # compress
        cfile = zstd(file)
        cstat = os.stat(cfile)
        compression_meta: t.Dict[str, t.Any] = {"type": "zstd", "size": cstat.st_size}
        if cstat.st_size < stat.st_size:
            if rm:
                os.remove(file)
            file = cfile
            metadata["compression"] = compression_meta
            logger.debug("Compressed with zstd.", file=file)
        else:
            os.remove(cfile)
            metadata["compression"] = None
            logger.debug("Compression ineffective.", file=file)

        # encrypt
        pubkey, privkey = age_keygen()
        encryption_meta = {"type": "age", "key": privkey}
        efile = age(file, (pubkey,))
        if rm or file == cfile:
            os.remove(file)
        file = efile
        metadata["encryption"] = encryption_meta
        logger.debug("Encrypted with age.", file=file)

        # hash again
        final_hashes = [
            hashlib.md5(),
            hashlib.sha1(),
            hashlib.sha256(),
        ]
        with open(file, "rb") as f:
            while data := f.read(1024 * 1024):
                for hash in final_hashes:
                    hash.update(data)
        metadata["hash"] = {h.name: h.hexdigest() for h in final_hashes}

        # rename blobs
        if rename_uuid or rename_hash:
            os.makedirs(out_dir, exist_ok=True)
            if rename_hash:
                os.rename(
                    # hash before compression + encryption, for deduplication purposes
                    file,
                    os.path.join(out_dir, metadata["file"]["hash"]["sha256"]),
                )
            if rename_uuid:
                os.rename(file, os.path.join(out_dir, metadata["uuid"]))

        # write metadata
        with open(metadata_file, "xb") as f:
            f.write(orjson.dumps(metadata))
