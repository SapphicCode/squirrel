import re
import subprocess
import typing as t

import orjson


def exiftool(path: str) -> dict:
    return orjson.loads(subprocess.check_output(["exiftool", "-json", path]))[0]


def zstd(path: str, level=3) -> str:
    newpath = f"{path}.zst"
    subprocess.check_call(("zstd", "-q", f"-{level}", path, f"-o={newpath}"))
    return newpath


def age_keygen() -> t.Tuple[str, str]:
    """
    Generates age public/private keypair.

    :returns: public key, private key
    """
    keys = subprocess.check_output(("age-keygen",), stderr=subprocess.DEVNULL).decode()
    pubkey, privkey = re.findall(r"public key: (age.+)\n(AGE-SECRET-KEY-.+)", keys)[0]
    assert isinstance(pubkey, str)
    assert isinstance(privkey, str)
    return pubkey, privkey


def age(path: str, recipients: t.Iterable[str], *, output: t.Optional[str] = None) -> str:
    newpath = output or f"{path}.age"
    args = ["age", "--encrypt", f"-o={newpath}"]
    for recipient in recipients:
        args.append(f"-r={recipient}")
    args.append(path)
    subprocess.check_call(args)
    return newpath


def age_inline(data: bytes, recipients: t.Iterable[str]) -> bytes:
    args = ["age", "--encrypt", "--armor"]
    for recipient in recipients:
        args.append(f"-r={recipient}")
    proc = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    out, _ = proc.communicate(data)
    assert proc.returncode == 0
    return out
